package co.com.edu.electrodomestico.aplicacion.comando;

import co.com.edu.electrodomestico.dominio.modelo.TipoIdentificacion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ComandoCliente {

    private Long id;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String numeroIdentificacion;
    private TipoIdentificacion tipoIdentificacion;
    private String numeroTelefono;
}
