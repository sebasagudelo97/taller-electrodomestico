package co.com.edu.electrodomestico.aplicacion.comando;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ComandoTipoElectrodomestico {

    private Long id;
    private String marca;
    private String descripcion;

}
