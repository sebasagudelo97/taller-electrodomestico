package co.com.edu.electrodomestico.aplicacion.consulta.manejador.cliente;

import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import co.com.edu.electrodomestico.dominio.repositoriopuerto.RepositorioCliente;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ManejadorListarCliente {

    private final RepositorioCliente repositorioCliente;

    public ManejadorListarCliente(RepositorioCliente repositorioCliente) {
        this.repositorioCliente = repositorioCliente;
    }
    public List<ClienteDto> ejecutar (){
        return this.repositorioCliente.listar();
    }
}
