package co.com.edu.electrodomestico.aplicacion.consulta.manejador.tipoElectrodomestico;

import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import co.com.edu.electrodomestico.dominio.modelo.dto.TipoElectrodomesticoDto;
import co.com.edu.electrodomestico.dominio.repositoriopuerto.RepositorioTipoElectrodomestico;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ManejadorListarTipoElectrodomestico {

    RepositorioTipoElectrodomestico repositorioTipoElectrodomestico;

    public ManejadorListarTipoElectrodomestico(RepositorioTipoElectrodomestico repositorioTipoElectrodomestico) {
        this.repositorioTipoElectrodomestico = repositorioTipoElectrodomestico;
    }

    public List<TipoElectrodomesticoDto> ejecutar (){
        return this.repositorioTipoElectrodomestico.listar();
    }
}
