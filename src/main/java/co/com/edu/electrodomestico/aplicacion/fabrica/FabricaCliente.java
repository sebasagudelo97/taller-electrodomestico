package co.com.edu.electrodomestico.aplicacion.fabrica;

import co.com.edu.electrodomestico.aplicacion.comando.ComandoCliente;
import co.com.edu.electrodomestico.dominio.modelo.Cliente;
import org.springframework.stereotype.Component;

@Component
public class FabricaCliente {

    public Cliente crear(ComandoCliente comandoCliente){
        return new Cliente(comandoCliente.getId(),comandoCliente.getPrimerNombre(),comandoCliente.getSegundoNombre(),
                           comandoCliente.getPrimerApellido(),comandoCliente.getSegundoApellido(),
                           comandoCliente.getNumeroIdentificacion(),comandoCliente.getTipoIdentificacion(),
                           comandoCliente.getNumeroTelefono());
    }
}
