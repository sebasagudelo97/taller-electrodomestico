package co.com.edu.electrodomestico.aplicacion.fabrica;

import co.com.edu.electrodomestico.aplicacion.comando.ComandoTipoElectrodomestico;
import co.com.edu.electrodomestico.dominio.modelo.TipoElectrodomestico;
import org.springframework.stereotype.Component;

@Component
public class FabricaTipoElectrodomestico {

     public TipoElectrodomestico crear(ComandoTipoElectrodomestico comandoTipoElectrodomestico){
         return new TipoElectrodomestico(comandoTipoElectrodomestico.getId(),comandoTipoElectrodomestico.getMarca(),comandoTipoElectrodomestico.getDescripcion());
     }
}
