package co.com.edu.electrodomestico.aplicacion.manejador.cliente;

import co.com.edu.electrodomestico.aplicacion.comando.ComandoCliente;
import co.com.edu.electrodomestico.aplicacion.fabrica.FabricaCliente;
import co.com.edu.electrodomestico.dominio.modelo.Cliente;
import co.com.edu.electrodomestico.dominio.servicio.cliente.ServicioRegistrarCliente;
import org.springframework.stereotype.Component;

@Component
public class ManejadorRegistrarCliente {

    private final ServicioRegistrarCliente servicioRegistrarCliente;
    private final FabricaCliente fabricaCliente;

    public ManejadorRegistrarCliente(ServicioRegistrarCliente servicioRegistrarCliente, FabricaCliente fabricaCliente) {
        this.servicioRegistrarCliente = servicioRegistrarCliente;
        this.fabricaCliente = fabricaCliente;
    }

    public void ejecutar(ComandoCliente comandoCliente){
        Cliente cliente = this.fabricaCliente.crear(comandoCliente);
        this.servicioRegistrarCliente.ejecutar(cliente);
    }
}
