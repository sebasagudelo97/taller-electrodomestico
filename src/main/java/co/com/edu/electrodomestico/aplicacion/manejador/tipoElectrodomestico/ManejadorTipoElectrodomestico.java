package co.com.edu.electrodomestico.aplicacion.manejador.tipoElectrodomestico;

import co.com.edu.electrodomestico.aplicacion.comando.ComandoTipoElectrodomestico;
import co.com.edu.electrodomestico.aplicacion.fabrica.FabricaTipoElectrodomestico;
import co.com.edu.electrodomestico.dominio.modelo.TipoElectrodomestico;
import co.com.edu.electrodomestico.dominio.servicio.tipoElectrodomestico.ServicioRegistrarTipoElectrodomestico;
import org.springframework.stereotype.Component;

@Component
public class ManejadorTipoElectrodomestico {

    private final ServicioRegistrarTipoElectrodomestico servicioRegistrarTipoElectrodomestico;
    private final FabricaTipoElectrodomestico fabricaTipoElectrodomestico;

    public ManejadorTipoElectrodomestico(ServicioRegistrarTipoElectrodomestico servicioRegistrarTipoElectrodomestico, FabricaTipoElectrodomestico fabricaTipoElectrodomestico) {
        this.servicioRegistrarTipoElectrodomestico = servicioRegistrarTipoElectrodomestico;
        this.fabricaTipoElectrodomestico = fabricaTipoElectrodomestico;
    }

    public void ejecutar (ComandoTipoElectrodomestico comandoTipoElectrodomestico){
        TipoElectrodomestico tipoElectrodomestico = this.fabricaTipoElectrodomestico.crear(comandoTipoElectrodomestico);
        this.servicioRegistrarTipoElectrodomestico.ejecutar(tipoElectrodomestico);
    }
}
