package co.com.edu.electrodomestico.dominio;

import co.com.edu.electrodomestico.dominio.excepcion.ExcepcionCampoObligatorio;
import co.com.edu.electrodomestico.dominio.excepcion.ExcepcionFechaInvalida;
import co.com.edu.electrodomestico.dominio.excepcion.ExcepcionLongitudIncorrecta;

import java.time.LocalDate;

public class  ValidadorArgumento {

    private static final int VALOR_MINIMO=3;
    private static final int VALOR_MAXIMO=16;

    public static void validarCampoObligatorio(Object valor, String mensaje){
        if(valor == null){
            throw new ExcepcionCampoObligatorio(mensaje);
        }
    }

    public static void validarLongitudCorrecta(String campoValidar, String mensaje){
        if(campoValidar.length() <= VALOR_MINIMO){
            throw new ExcepcionLongitudIncorrecta(mensaje);
        }else if(campoValidar.length() > VALOR_MAXIMO ){
            throw new ExcepcionLongitudIncorrecta(mensaje);
        }
    }

    public static void validarFechaCorrecta(LocalDate fechaIngreso, LocalDate fechaSalida, String mensaje){
        if(fechaSalida.isBefore(fechaIngreso)){
            throw new ExcepcionFechaInvalida(mensaje);
        }
    }
}
