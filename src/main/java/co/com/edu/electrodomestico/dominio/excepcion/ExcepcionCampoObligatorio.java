package co.com.edu.electrodomestico.dominio.excepcion;

public class ExcepcionCampoObligatorio extends RuntimeException {

    public ExcepcionCampoObligatorio(String message) {
        super(message);
    }
}
