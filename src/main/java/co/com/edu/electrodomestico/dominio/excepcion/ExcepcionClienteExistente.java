package co.com.edu.electrodomestico.dominio.excepcion;

public class ExcepcionClienteExistente extends RuntimeException {

    public ExcepcionClienteExistente(String message) {
        super(message);
    }
}
