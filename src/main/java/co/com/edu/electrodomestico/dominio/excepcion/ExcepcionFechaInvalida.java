package co.com.edu.electrodomestico.dominio.excepcion;

public class ExcepcionFechaInvalida extends RuntimeException {

    public ExcepcionFechaInvalida(String message) {
        super(message);
    }
}
