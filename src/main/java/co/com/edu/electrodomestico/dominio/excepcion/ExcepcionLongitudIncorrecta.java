package co.com.edu.electrodomestico.dominio.excepcion;

public class ExcepcionLongitudIncorrecta extends RuntimeException {

    public ExcepcionLongitudIncorrecta(String message) {
        super(message);
    }
}
