package co.com.edu.electrodomestico.dominio.excepcion;

public class ExcepcionTipoElectrodomesticoExistente extends RuntimeException {

    public ExcepcionTipoElectrodomesticoExistente(String message) {
        super(message);
    }
}
