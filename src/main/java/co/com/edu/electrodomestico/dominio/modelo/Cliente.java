package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.ValidadorArgumento;

public class Cliente {

    private static final String CAMPO_OBLIGATORIO= "El campo es obligatorio";
    private static final String VALOR_MINIMO="El valor minimo de caracteres es de 3 y el valor maximo de caracteres es de 16";

    private Long id;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String numeroIdentificacion;
    private TipoIdentificacion tipoIdentificacion;
    private String numeroTelefono;

    public Cliente(Long id, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String numeroIdentificacion, TipoIdentificacion tipoIdentificacion, String numeroTelefono) {
        ValidadorArgumento.validarCampoObligatorio(primerNombre, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarCampoObligatorio(primerApellido, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarCampoObligatorio(numeroIdentificacion, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarCampoObligatorio(tipoIdentificacion, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarCampoObligatorio(numeroTelefono, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarLongitudCorrecta(primerNombre, VALOR_MINIMO);
        ValidadorArgumento.validarLongitudCorrecta(primerApellido, VALOR_MINIMO);
        ValidadorArgumento.validarLongitudCorrecta(numeroIdentificacion, VALOR_MINIMO);
        ValidadorArgumento.validarLongitudCorrecta(numeroTelefono, VALOR_MINIMO);

        this.id = id;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.numeroIdentificacion = numeroIdentificacion;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroTelefono = numeroTelefono;
    }


    public Long getId() {
        return id;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public TipoIdentificacion getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }
}
