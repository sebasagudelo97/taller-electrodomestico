package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.ValidadorArgumento;

public class MovimientoElectrodomestico {
    private static final String CAMPO_OBLIGATORIO="Este campo es obligatorio";

    private Long id;
    private RegistroElectrodomestico registroElectrodomestico;
    private String informacionSalida;
    private boolean estado;

    public MovimientoElectrodomestico(Long id, RegistroElectrodomestico registroElectrodomestico, String informacionSalida, boolean estado) {
        ValidadorArgumento.validarCampoObligatorio(informacionSalida, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarCampoObligatorio(registroElectrodomestico, CAMPO_OBLIGATORIO);

        this.id = id;
        this.registroElectrodomestico=registroElectrodomestico;
        this.informacionSalida = informacionSalida;
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public RegistroElectrodomestico getRegistroElectrodomestico() {
        return registroElectrodomestico;
    }

    public String getInformacionSalida() {
        return informacionSalida;
    }

    public boolean isEstado() {
        return estado;
    }
}
