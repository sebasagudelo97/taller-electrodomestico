package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.ValidadorArgumento;

import java.time.LocalDate;

public class RegistroElectrodomestico {

    private static final String CAMPO_OBLIGATORIO="Este campo es obligatorio";

    private Long id;
    private Cliente cliente;
    private LocalDate fechaIngreso;
    private TipoElectrodomestico tipoElectrodomestico;
    private String reporteDaño;
    private LocalDate fechaSalida;

    public RegistroElectrodomestico(Long id, Cliente cliente, LocalDate fechaIngreso, TipoElectrodomestico tipoElectrodomestico, String reporteDaño, LocalDate fechaSalida) {
        ValidadorArgumento.validarCampoObligatorio(cliente, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarCampoObligatorio(tipoElectrodomestico, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarCampoObligatorio(reporteDaño,CAMPO_OBLIGATORIO);

        this.id = id;
        this.cliente = cliente;
        this.fechaIngreso = fechaIngreso;
        this.tipoElectrodomestico = tipoElectrodomestico;
        this.reporteDaño = reporteDaño;
        this.fechaSalida = fechaSalida;
    }

    public Long getId() {
        return id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public TipoElectrodomestico getTipoElectrodomestico() {
        return tipoElectrodomestico;
    }

    public String getReporteDaño() {
        return reporteDaño;
    }

    public LocalDate getFechaSalida() {
        return fechaSalida;
    }
}
