package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.ValidadorArgumento;

public class TipoElectrodomestico {

    private static final String CAMPO_OBLIGATORIO= "El campo es obligatorio";

    private Long id;
    private String marca;
    private String descripcion;

    public TipoElectrodomestico(Long id, String marca, String descripcion) {
        ValidadorArgumento.validarCampoObligatorio(marca, CAMPO_OBLIGATORIO);
        ValidadorArgumento.validarCampoObligatorio(descripcion, CAMPO_OBLIGATORIO);

        this.id = id;
        this.marca = marca;
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public String getMarca() {
        return marca;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
