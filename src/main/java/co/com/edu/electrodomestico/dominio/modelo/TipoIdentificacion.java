package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.ValidadorArgumento;

public class TipoIdentificacion {
    private static final String CAMPO_OBLIGATORIO= "El campo es obligatorio";

    private Long id;
    private String descripcion;

    public TipoIdentificacion(Long id, String descripcion) {
        ValidadorArgumento.validarCampoObligatorio(descripcion, CAMPO_OBLIGATORIO);

        this.id = id;
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
