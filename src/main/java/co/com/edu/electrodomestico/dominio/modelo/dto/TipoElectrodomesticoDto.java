package co.com.edu.electrodomestico.dominio.modelo.dto;

public class TipoElectrodomesticoDto {

    private Long id;
    private String marca;
    private String descripcion;

    public TipoElectrodomesticoDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
