package co.com.edu.electrodomestico.dominio.repositoriopuerto;

import co.com.edu.electrodomestico.dominio.modelo.Cliente;
import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioCliente {

    void guardar(Cliente cliente);
    boolean existe(Cliente cliente);
    List<ClienteDto> listar();

}
