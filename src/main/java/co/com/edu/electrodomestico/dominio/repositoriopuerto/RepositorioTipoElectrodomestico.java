package co.com.edu.electrodomestico.dominio.repositoriopuerto;

import co.com.edu.electrodomestico.dominio.modelo.TipoElectrodomestico;
import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import co.com.edu.electrodomestico.dominio.modelo.dto.TipoElectrodomesticoDto;

import java.util.List;

public interface RepositorioTipoElectrodomestico {

    void guardar(TipoElectrodomestico tipoElectrodomestico);
    boolean existe ( TipoElectrodomestico tipoElectrodomestico);
    List<TipoElectrodomesticoDto> listar();
}
