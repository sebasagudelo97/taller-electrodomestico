package co.com.edu.electrodomestico.dominio.repositoriopuerto;

import co.com.edu.electrodomestico.infraestructura.entidad.TipoIdentificacionEntidad;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioTipoIdentificacion {

    TipoIdentificacionEntidad filtrarPorId(Long id);
}
