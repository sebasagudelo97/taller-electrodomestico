package co.com.edu.electrodomestico.dominio.servicio.cliente;

import co.com.edu.electrodomestico.dominio.excepcion.ExcepcionClienteExistente;
import co.com.edu.electrodomestico.dominio.modelo.Cliente;
import co.com.edu.electrodomestico.dominio.repositoriopuerto.RepositorioCliente;
import org.springframework.stereotype.Service;

@Service
public class ServicioRegistrarCliente {

    private final String CLIENTE_EXISTENTE="El cliente ya se encuentra registrado en el sistema";

    private final RepositorioCliente repositorioCliente;

    public ServicioRegistrarCliente(RepositorioCliente repositorioCliente) {
        this.repositorioCliente = repositorioCliente;
    }

    private void validarExistenciaCliente(Cliente cliente){
        boolean clienteExiste=repositorioCliente.existe(cliente);
        if (clienteExiste){
            throw new ExcepcionClienteExistente(CLIENTE_EXISTENTE);
        }
    }

    public void ejecutar(Cliente cliente){
        validarExistenciaCliente(cliente);
        repositorioCliente.guardar(cliente);
    }
}
