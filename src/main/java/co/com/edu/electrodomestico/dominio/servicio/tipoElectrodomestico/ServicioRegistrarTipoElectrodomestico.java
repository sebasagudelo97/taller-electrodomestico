package co.com.edu.electrodomestico.dominio.servicio.tipoElectrodomestico;

import co.com.edu.electrodomestico.dominio.excepcion.ExcepcionTipoElectrodomesticoExistente;
import co.com.edu.electrodomestico.dominio.modelo.TipoElectrodomestico;
import co.com.edu.electrodomestico.dominio.repositoriopuerto.RepositorioTipoElectrodomestico;
import org.springframework.stereotype.Service;

@Service
public class ServicioRegistrarTipoElectrodomestico {

    private final String TIPO_ELECTRODOMESTICO_EXISTENTE="El tipo de electrodomestico ya se encuentra registrado en el sistema";
    private RepositorioTipoElectrodomestico repositorioTipoElectrodomestico;

    public ServicioRegistrarTipoElectrodomestico(RepositorioTipoElectrodomestico repositorioTipoElectrodomestico) {
        this.repositorioTipoElectrodomestico = repositorioTipoElectrodomestico;
    }

    private void validarExistenciaTipoElectrodomestico(TipoElectrodomestico tipoElectrodomestico){
        boolean tipoElectrodomesticoExiste=repositorioTipoElectrodomestico.existe(tipoElectrodomestico);
        if (tipoElectrodomesticoExiste){
            throw new ExcepcionTipoElectrodomesticoExistente(TIPO_ELECTRODOMESTICO_EXISTENTE);
        }
    }
    public void ejecutar(TipoElectrodomestico tipoElectrodomestico){
        validarExistenciaTipoElectrodomestico(tipoElectrodomestico);
        this.repositorioTipoElectrodomestico.guardar(tipoElectrodomestico);
    }

}
