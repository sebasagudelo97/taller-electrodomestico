package co.com.edu.electrodomestico.infraestructura.controlador;

import co.com.edu.electrodomestico.aplicacion.comando.ComandoCliente;
import co.com.edu.electrodomestico.aplicacion.consulta.manejador.cliente.ManejadorListarCliente;
import co.com.edu.electrodomestico.aplicacion.manejador.cliente.ManejadorRegistrarCliente;
import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/cliente")
public class ControladorCliente {

    private final ManejadorRegistrarCliente manejadorRegistrarCliente;
    private final ManejadorListarCliente manejadorListarCliente;

    public ControladorCliente(ManejadorRegistrarCliente manejadorRegistrarCliente, ManejadorListarCliente manejadorListarCliente) {
        this.manejadorRegistrarCliente = manejadorRegistrarCliente;
        this.manejadorListarCliente = manejadorListarCliente;
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus(HttpStatus.OK)
    public void guardarCliente(@RequestBody ComandoCliente comandoCliente){
        this.manejadorRegistrarCliente.ejecutar(comandoCliente);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ClienteDto> listarCliente(){
        return this.manejadorListarCliente.ejecutar();

    }
}
