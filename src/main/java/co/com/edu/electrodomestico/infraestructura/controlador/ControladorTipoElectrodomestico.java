package co.com.edu.electrodomestico.infraestructura.controlador;

import co.com.edu.electrodomestico.aplicacion.comando.ComandoTipoElectrodomestico;
import co.com.edu.electrodomestico.aplicacion.consulta.manejador.tipoElectrodomestico.ManejadorListarTipoElectrodomestico;
import co.com.edu.electrodomestico.aplicacion.manejador.tipoElectrodomestico.ManejadorTipoElectrodomestico;
import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import co.com.edu.electrodomestico.dominio.modelo.dto.TipoElectrodomesticoDto;
import co.com.edu.electrodomestico.dominio.servicio.tipoElectrodomestico.ServicioRegistrarTipoElectrodomestico;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/tipoElectrodomestico")
public class ControladorTipoElectrodomestico {

    private final ManejadorTipoElectrodomestico manejadorTipoElectrodomestico;
    private final ManejadorListarTipoElectrodomestico manejadorListarTipoElectrodomestico;

    public ControladorTipoElectrodomestico(ManejadorTipoElectrodomestico manejadorTipoElectrodomestico, ManejadorListarTipoElectrodomestico manejadorListarTipoElectrodomestico) {
        this.manejadorTipoElectrodomestico = manejadorTipoElectrodomestico;
        this.manejadorListarTipoElectrodomestico = manejadorListarTipoElectrodomestico;
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void guardarTipoElectrodomestico(@RequestBody ComandoTipoElectrodomestico comandoTipoElectrodomestico){
        this.manejadorTipoElectrodomestico.ejecutar(comandoTipoElectrodomestico);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<TipoElectrodomesticoDto> listarTipoElectrodomestico(){
        return this.manejadorListarTipoElectrodomestico.ejecutar();
    }
}
