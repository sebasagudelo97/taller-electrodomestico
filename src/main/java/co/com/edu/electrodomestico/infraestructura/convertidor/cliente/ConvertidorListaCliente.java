package co.com.edu.electrodomestico.infraestructura.convertidor.cliente;

import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import co.com.edu.electrodomestico.infraestructura.entidad.ClienteEntidad;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConvertidorListaCliente {

    private ModelMapper modelMapper = new ModelMapper();

    public List<ClienteDto> convertirListClienteEntidadAListCliente(List<ClienteEntidad> listClienteEntidad, List<ClienteDto> listClienteDto){
        for(ClienteEntidad clienteEntidad : listClienteEntidad){
            ClienteDto clienteDto = modelMapper.map(clienteEntidad,ClienteDto.class);
            listClienteDto.add(clienteDto);
        }
        return listClienteDto;
    }
}
