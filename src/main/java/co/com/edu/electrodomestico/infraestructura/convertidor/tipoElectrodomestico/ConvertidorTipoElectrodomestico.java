package co.com.edu.electrodomestico.infraestructura.convertidor.tipoElectrodomestico;

import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import co.com.edu.electrodomestico.dominio.modelo.dto.TipoElectrodomesticoDto;
import co.com.edu.electrodomestico.infraestructura.entidad.ClienteEntidad;
import co.com.edu.electrodomestico.infraestructura.entidad.TipoElectrodomesticoEntidad;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConvertidorTipoElectrodomestico {

    private ModelMapper modelMapper = new ModelMapper();

    public List<TipoElectrodomesticoDto> convertirListTipoElectrodomesticoEntidadAListTipoElectrodomestico(List<TipoElectrodomesticoEntidad> listTipoElectrodomesticoEntidad, List<TipoElectrodomesticoDto> listtipoElectrodomesticoDto){
        for(TipoElectrodomesticoEntidad tipoElectrodomesticoEntidad : listTipoElectrodomesticoEntidad){
            TipoElectrodomesticoDto tipoElectrodomesticoDto = modelMapper.map(tipoElectrodomesticoEntidad,TipoElectrodomesticoDto.class);
            listtipoElectrodomesticoDto.add(tipoElectrodomesticoDto);
        }
        return listtipoElectrodomesticoDto;
    }
}
