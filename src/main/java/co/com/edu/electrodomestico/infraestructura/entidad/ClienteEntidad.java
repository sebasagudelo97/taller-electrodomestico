package co.com.edu.electrodomestico.infraestructura.entidad;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cliente_entidad")
public class ClienteEntidad {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "primerNombre", nullable = false, length = 300)
    private String primerNombre;

    @Column(name = "segundoNombre", length = 300)
    private String segundoNombre;

    @Column(name = "primerApellido", nullable = false, length = 300)
    private String primerApellido;

    @Column(name = "segundoApellido", length = 300)
    private String segundoApellido;

    @Column(name = "numeroIdentificacion", length = 300, nullable = false)
    private String numeroIdentificacion;

    @ManyToOne
    @JoinColumn
    private TipoIdentificacionEntidad tipoIdentificacion;

    @Column(name = "numeroTelefono", nullable = false, length = 300)
    private String numeroTelefono;


}
