package co.com.edu.electrodomestico.infraestructura.entidad;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor
@Table(name = "movimientoElectrodomesticoEntidad")
public class MovimientoElectrodomesticoEntidad {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    private Long id;

    @ManyToOne
    @JoinColumn
    private RegistroElectrodomesticoEntidad registroElectrodomestico;

    @Column(name = "informacion_salida", nullable = false, length = 300)
    private String informacionSalida;

    @Column(name = "estado", nullable = false)
    private boolean estado;
}
