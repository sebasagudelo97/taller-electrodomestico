package co.com.edu.electrodomestico.infraestructura.entidad;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor
@Table(name = "registroElectrodomesticoEntidad")
public class RegistroElectrodomesticoEntidad {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    private Long id;

    @ManyToOne
    @JoinColumn
    private ClienteEntidad cliente;

    @Column(name = "fecha_ingreso", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd")
    private LocalDate fechaIngreso;

    @ManyToMany
    @JoinTable(name = "relRegistroElectrodomesticoTipoElectrodomestico", joinColumns = @JoinColumn
    , inverseJoinColumns = @JoinColumn)
    private List<TipoElectrodomesticoEntidad> tipoElectrodomestico;

    @Column(name = "reporte_daño")
    private String reporteDaño;

    @Column(name = "fecha_salida", nullable = true)
    private LocalDate fechaSalida;
}
