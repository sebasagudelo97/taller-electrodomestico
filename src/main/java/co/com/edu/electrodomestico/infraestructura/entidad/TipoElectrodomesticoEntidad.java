package co.com.edu.electrodomestico.infraestructura.entidad;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tipoElectrodomesticoEntidad")
public class TipoElectrodomesticoEntidad {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    private Long id;

    @Column(name = "marca", nullable = false, length = 255)
    private String marca;

    @Column(name = "descripcion", nullable = false, length = 255)
    private String descripcion;

}
