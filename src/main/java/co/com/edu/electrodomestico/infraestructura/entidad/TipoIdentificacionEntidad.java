package co.com.edu.electrodomestico.infraestructura.entidad;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tipoIdentificacionEntidad")
public class TipoIdentificacionEntidad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    private Long id;

    @Column(name = "descripcion", length = 300, nullable = false)
    private String descripcion;

}
