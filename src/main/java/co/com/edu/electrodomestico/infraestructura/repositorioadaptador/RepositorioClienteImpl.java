package co.com.edu.electrodomestico.infraestructura.repositorioadaptador;

import co.com.edu.electrodomestico.dominio.modelo.Cliente;
import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import co.com.edu.electrodomestico.dominio.repositoriopuerto.RepositorioCliente;
import co.com.edu.electrodomestico.infraestructura.convertidor.cliente.ConvertidorListaCliente;
import co.com.edu.electrodomestico.infraestructura.entidad.ClienteEntidad;
import co.com.edu.electrodomestico.infraestructura.repositoriojpa.RepositorioClienteJpa;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RepositorioClienteImpl implements RepositorioCliente {

    private final RepositorioClienteJpa repositorioClienteJpa;
    private final ModelMapper modelMapper= new ModelMapper();
    ConvertidorListaCliente convertidorListaCliente= new ConvertidorListaCliente();

    public RepositorioClienteImpl(RepositorioClienteJpa repositorioClienteJpa) {
        this.repositorioClienteJpa = repositorioClienteJpa;
    }

    @Override
    public void guardar(Cliente cliente) {
        ClienteEntidad clienteEntidad= modelMapper.map(cliente, ClienteEntidad.class);
        repositorioClienteJpa.save(clienteEntidad);
    }

    @Override
    public boolean existe(Cliente cliente) {
        return repositorioClienteJpa.filtrarClientePorNumeroIdentificacion(cliente.getNumeroIdentificacion()) != null;
    }

    @Override
    public List<ClienteDto> listar() {
        List<ClienteEntidad> listClienteEntidad = repositorioClienteJpa.findAll();
        List<ClienteDto> listClienteDto= new ArrayList<>();
        return convertidorListaCliente.convertirListClienteEntidadAListCliente(listClienteEntidad,listClienteDto);
    }
}
