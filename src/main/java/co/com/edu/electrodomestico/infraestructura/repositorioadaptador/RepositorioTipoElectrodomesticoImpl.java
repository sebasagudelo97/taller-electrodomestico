package co.com.edu.electrodomestico.infraestructura.repositorioadaptador;

import co.com.edu.electrodomestico.dominio.modelo.TipoElectrodomestico;
import co.com.edu.electrodomestico.dominio.modelo.dto.ClienteDto;
import co.com.edu.electrodomestico.dominio.modelo.dto.TipoElectrodomesticoDto;
import co.com.edu.electrodomestico.dominio.repositoriopuerto.RepositorioTipoElectrodomestico;
import co.com.edu.electrodomestico.infraestructura.convertidor.tipoElectrodomestico.ConvertidorTipoElectrodomestico;
import co.com.edu.electrodomestico.infraestructura.entidad.ClienteEntidad;
import co.com.edu.electrodomestico.infraestructura.entidad.TipoElectrodomesticoEntidad;
import co.com.edu.electrodomestico.infraestructura.repositoriojpa.RepositorioTipoElectrodomesticoJpa;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RepositorioTipoElectrodomesticoImpl implements RepositorioTipoElectrodomestico {

    private final RepositorioTipoElectrodomesticoJpa repositorioTipoElectrodomesticoJpa;
    private final ModelMapper modelMapper= new ModelMapper();
    ConvertidorTipoElectrodomestico convertidorTipoElectrodomestico = new ConvertidorTipoElectrodomestico();
    public RepositorioTipoElectrodomesticoImpl(RepositorioTipoElectrodomesticoJpa repositorioTipoElectrodomesticoJpa) {
        this.repositorioTipoElectrodomesticoJpa = repositorioTipoElectrodomesticoJpa;
    }

    @Override
    public void guardar(TipoElectrodomestico tipoElectrodomestico) {
        TipoElectrodomesticoEntidad tipoElectrodomesticoEntidad= modelMapper.map(tipoElectrodomestico,TipoElectrodomesticoEntidad.class);
        repositorioTipoElectrodomesticoJpa.save(tipoElectrodomesticoEntidad);
    }

    @Override
    public boolean existe(TipoElectrodomestico tipoElectrodomestico) {
       return repositorioTipoElectrodomesticoJpa.filtrarTipoElectrodomesticoPorMarca(tipoElectrodomestico.getMarca()) !=null;
    }

    @Override
    public List<TipoElectrodomesticoDto> listar() {
        List<TipoElectrodomesticoEntidad> listTipoElectrodomesticoEntidad = repositorioTipoElectrodomesticoJpa.findAll();
        List<TipoElectrodomesticoDto> listTipoElectrodomesticoDto= new ArrayList<>();
        return convertidorTipoElectrodomestico.convertirListTipoElectrodomesticoEntidadAListTipoElectrodomestico(listTipoElectrodomesticoEntidad,listTipoElectrodomesticoDto);
    }

}
