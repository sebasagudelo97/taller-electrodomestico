package co.com.edu.electrodomestico.infraestructura.repositorioadaptador;

import co.com.edu.electrodomestico.dominio.repositoriopuerto.RepositorioTipoIdentificacion;
import co.com.edu.electrodomestico.infraestructura.entidad.TipoIdentificacionEntidad;
import co.com.edu.electrodomestico.infraestructura.repositoriojpa.RepositorioTipoIdentificacionJpa;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

@Repository
public class RepositorioTipoIdentificacionImpl implements RepositorioTipoIdentificacion {

    private RepositorioTipoIdentificacionJpa repositorioTipoIdentificacionJpa;

    ModelMapper modelMapper= new ModelMapper();

    public RepositorioTipoIdentificacionImpl(RepositorioTipoIdentificacionJpa repositorioTipoIdentificacionJpa) {
        this.repositorioTipoIdentificacionJpa = repositorioTipoIdentificacionJpa;
    }

    @Override
    public TipoIdentificacionEntidad filtrarPorId(Long id) {
        return  repositorioTipoIdentificacionJpa.filtrarTipoIdentificacionPorId(id);
    }
}
