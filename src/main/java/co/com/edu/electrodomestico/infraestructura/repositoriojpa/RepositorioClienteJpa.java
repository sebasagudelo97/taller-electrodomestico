package co.com.edu.electrodomestico.infraestructura.repositoriojpa;

import co.com.edu.electrodomestico.infraestructura.entidad.ClienteEntidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface RepositorioClienteJpa extends JpaRepository<ClienteEntidad, Serializable>{


     @Query(value = "select * from cliente_entidad where numero_identificacion =:numero_identificacion", nativeQuery = true)
     ClienteEntidad filtrarClientePorNumeroIdentificacion(@Param("numero_identificacion") String numero_identificacion);

     
}
