package co.com.edu.electrodomestico.infraestructura.repositoriojpa;

import co.com.edu.electrodomestico.infraestructura.entidad.ClienteEntidad;
import co.com.edu.electrodomestico.infraestructura.entidad.TipoElectrodomesticoEntidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface RepositorioTipoElectrodomesticoJpa extends JpaRepository<TipoElectrodomesticoEntidad, Serializable> {


    @Query(value = "select * from tipo_electrodomestico_entidad where marca =:marca", nativeQuery = true)
    TipoElectrodomesticoEntidad filtrarTipoElectrodomesticoPorMarca(@Param("marca") String marca);
}
