package co.com.edu.electrodomestico.infraestructura.repositoriojpa;

import co.com.edu.electrodomestico.infraestructura.entidad.TipoIdentificacionEntidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface RepositorioTipoIdentificacionJpa extends JpaRepository<TipoIdentificacionEntidad, Serializable> {

    @Query(value = "select id from tipo_identificacion_entidad where id =:id", nativeQuery = true)
    TipoIdentificacionEntidad filtrarTipoIdentificacionPorId(@Param("id") Long id);

}
