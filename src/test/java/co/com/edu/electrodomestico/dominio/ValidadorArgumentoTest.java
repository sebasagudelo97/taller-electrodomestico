package co.com.edu.electrodomestico.dominio;

import co.com.edu.electrodomestico.dominio.excepcion.ExcepcionCampoObligatorio;
import co.com.edu.electrodomestico.dominio.excepcion.ExcepcionFechaInvalida;
import co.com.edu.electrodomestico.dominio.excepcion.ExcepcionLongitudIncorrecta;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

 class ValidadorArgumentoTest {

    private static final String CAMPO_OBLIGATORIO= "El campo es obligatorio";
    private static final LocalDate FECHA_ENTRADA= LocalDate.of(2020,5,10);
    private static final LocalDate FECHA_SALIDA = LocalDate.of(2020,5,5);
    private static final String FECHA_INVALIDA= "La fecha de salida no puede ser antes que la fecha de entrada";
    private static final String CAMPO_CON_MENOS_DE_TRES_DIGITOS="as";
    private static final String CAMPO_CON_MAS_DE_DIECISEIS_DIGITOS="asdjepjdslkalskas";
    private static final String VALOR_MINIMO="El valor minimo de caracteres es de 3 y el valor maximo de caracteres es de 16";

    @Test
    void campoObligatorioTest(){
        try {
            ValidadorArgumento.validarCampoObligatorio(null,CAMPO_OBLIGATORIO);
        }catch (ExcepcionCampoObligatorio e){
            Assertions.assertEquals(CAMPO_OBLIGATORIO, e.getMessage());
        }
    }

    @Test
    void fechaInvalidaTest(){
        try{
            ValidadorArgumento.validarFechaCorrecta(FECHA_ENTRADA,FECHA_SALIDA,FECHA_INVALIDA);
        }catch(ExcepcionFechaInvalida e){
            Assertions.assertEquals(FECHA_INVALIDA,e.getMessage());
        }
    }

    @Test
    void validarCampoConMenosDeTresDigitosTest(){
        try{
            ValidadorArgumento.validarLongitudCorrecta(CAMPO_CON_MENOS_DE_TRES_DIGITOS,VALOR_MINIMO);
        }catch (ExcepcionLongitudIncorrecta e ){
            Assertions.assertEquals(VALOR_MINIMO,e.getMessage());
        }
    }

    @Test
    void validarCampoConMasDeDieciseisDigitosTest(){
        try{
            ValidadorArgumento.validarLongitudCorrecta(CAMPO_CON_MAS_DE_DIECISEIS_DIGITOS,VALOR_MINIMO);
        }catch (ExcepcionLongitudIncorrecta e ){
            Assertions.assertEquals(VALOR_MINIMO,e.getMessage());
        }
    }

}
