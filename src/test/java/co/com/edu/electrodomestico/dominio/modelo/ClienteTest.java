package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.modelo.databuilder.ClienteTestDataBuilder;
import co.com.edu.electrodomestico.dominio.modelo.databuilder.TipoIdentificacionTestDataBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ClienteTest {

    private static final Long ID=1L;
    private static final String PRIMER_NOMBRE="Victor";
    private static final String SEGUNDO_NOMBRE="Manuel";
    private static final String PRIMER_APELLIDO="Vallejo";
    private static final String SEGUNDO_APELLIDO="Loaiza";
    private static final String NUMERO_IDENTIFICACION="123456789";
    private static final TipoIdentificacion TIPO_IDENTIFICACION= new TipoIdentificacionTestDataBuilder().builder();
    private static final String NUMERO_TELEFONO="3144568565";

    @Test
    void validarGetterClienteTest(){

        Cliente cliente = new ClienteTestDataBuilder().conTipoIdentificacion(TIPO_IDENTIFICACION).builder();

        Assertions.assertEquals(ID, cliente.getId());
        Assertions.assertEquals(PRIMER_NOMBRE,cliente.getPrimerNombre());
        Assertions.assertEquals(SEGUNDO_NOMBRE,cliente.getSegundoNombre());
        Assertions.assertEquals(PRIMER_APELLIDO,cliente.getPrimerApellido());
        Assertions.assertEquals(SEGUNDO_APELLIDO,cliente.getSegundoApellido());
        Assertions.assertEquals(NUMERO_IDENTIFICACION,cliente.getNumeroIdentificacion());
        Assertions.assertEquals(NUMERO_TELEFONO,cliente.getNumeroTelefono());
        Assertions.assertEquals(TIPO_IDENTIFICACION,cliente.getTipoIdentificacion());
    }
}
