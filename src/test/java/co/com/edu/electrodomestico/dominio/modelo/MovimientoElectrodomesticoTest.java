package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.modelo.databuilder.MovimientoElectrodomesticoTestDataBuilder;
import co.com.edu.electrodomestico.dominio.modelo.databuilder.RegistroElectrodomesticoTestDataBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MovimientoElectrodomesticoTest {

    private static final Long ID=1L;
    private static final RegistroElectrodomestico REGISTRO_ELECTRODOMESTICO=new RegistroElectrodomesticoTestDataBuilder().builder();
    private static final String INFORMACION_SALIDA="xxxmmmyyyy";
    private static final boolean ESTADO=true;

    @Test
    void validarGetterMovimientoElectrodomesticoTest(){
        MovimientoElectrodomestico movimientoElectrodomestico = new MovimientoElectrodomesticoTestDataBuilder().conRegistroElectrodomestico(REGISTRO_ELECTRODOMESTICO).builder();

        Assertions.assertEquals(ID,movimientoElectrodomestico.getId());
        Assertions.assertEquals(REGISTRO_ELECTRODOMESTICO,movimientoElectrodomestico.getRegistroElectrodomestico());
        Assertions.assertEquals(INFORMACION_SALIDA,movimientoElectrodomestico.getInformacionSalida());
        Assertions.assertEquals(ESTADO,movimientoElectrodomestico.isEstado());
    }
}
