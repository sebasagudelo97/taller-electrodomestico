package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.modelo.databuilder.ClienteTestDataBuilder;
import co.com.edu.electrodomestico.dominio.modelo.databuilder.RegistroElectrodomesticoTestDataBuilder;
import co.com.edu.electrodomestico.dominio.modelo.databuilder.TipoElectrodomesticoTestDataBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class RegistroElectrodomesticoTest {

    private static final Long ID=1L;
    private static final Cliente CLIENTE =new ClienteTestDataBuilder().builder();
    private static final LocalDate FECHA_INGRESO= LocalDate.of(2020,5,27);
    private static final TipoElectrodomestico TIPO_ELECTRODOMESTICO= new TipoElectrodomesticoTestDataBuilder().builder();
    private static final String REPORTE_DAÑO="xxxx";
    private static final LocalDate FECHA_SALIDA=LocalDate.of(2020,6,13);

    @Test
    void validarGetterRegistroElectrodomesticoTest(){

        RegistroElectrodomestico registroElectrodomestico= new RegistroElectrodomesticoTestDataBuilder().conCliente(CLIENTE).conTipoElectrodomestico(TIPO_ELECTRODOMESTICO).builder();

        Assertions.assertEquals(ID,registroElectrodomestico.getId());
        Assertions.assertEquals(CLIENTE,registroElectrodomestico.getCliente());
        Assertions.assertEquals(FECHA_INGRESO,registroElectrodomestico.getFechaIngreso());
        Assertions.assertEquals(TIPO_ELECTRODOMESTICO,registroElectrodomestico.getTipoElectrodomestico());
        Assertions.assertEquals(REPORTE_DAÑO,registroElectrodomestico.getReporteDaño());
        Assertions.assertEquals(FECHA_SALIDA,registroElectrodomestico.getFechaSalida());

    }
}
