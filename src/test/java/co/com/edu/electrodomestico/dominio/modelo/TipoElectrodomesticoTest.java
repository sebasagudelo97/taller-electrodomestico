package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.modelo.databuilder.TipoElectrodomesticoTestDataBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TipoElectrodomesticoTest {

    private static final Long ID=1L;
    private static final String MARCA="Inmusa";
    private static final String DESCRIPCION="Cafetera";

    @Test
    void validarGetterTipoElectrodomesticoTest(){

        TipoElectrodomestico tipoElectrodomestico= new TipoElectrodomesticoTestDataBuilder().builder();

        Assertions.assertEquals(ID,tipoElectrodomestico.getId());
        Assertions.assertEquals(MARCA,tipoElectrodomestico.getMarca());
        Assertions.assertEquals(DESCRIPCION,tipoElectrodomestico.getDescripcion());
    }
}
