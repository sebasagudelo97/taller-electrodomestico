package co.com.edu.electrodomestico.dominio.modelo;

import co.com.edu.electrodomestico.dominio.modelo.databuilder.TipoIdentificacionTestDataBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TipoIdentificacionTest {

    private static final Long ID=1L;
    private static final String DESCRIPCION="cedula de ciudadania";

    @Test
    void validadGetterTipoIdentificacionTest(){

        TipoIdentificacion tipoIdentificacion= new TipoIdentificacionTestDataBuilder().builder();

        Assertions.assertEquals(ID,tipoIdentificacion.getId());
        Assertions.assertEquals(DESCRIPCION,tipoIdentificacion.getDescripcion());
    }
}

