package co.com.edu.electrodomestico.dominio.modelo.databuilder;

import co.com.edu.electrodomestico.dominio.modelo.Cliente;
import co.com.edu.electrodomestico.dominio.modelo.TipoIdentificacion;

public class ClienteTestDataBuilder {

    private static final Long ID=1L;
    private static final String PRIMER_NOMBRE="Victor";
    private static final String SEGUNDO_NOMBRE="Manuel";
    private static final String PRIMER_APELLIDO="Vallejo";
    private static final String SEGUNDO_APELLIDO="Loaiza";
    private static final String NUMERO_IDENTIFICACION="123456789";
    private static final TipoIdentificacion TIPO_IDENTIFICACION= new TipoIdentificacion(1L,"cedula de ciudadania");
    private static final String NUMERO_TELEFONO="3144568565";

    private Long id;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String numeroIdentificacion;
    private TipoIdentificacion tipoIdentificacion;
    private String numeroTelefono;

    public ClienteTestDataBuilder() {
        this.id=ID;
        this.primerNombre=PRIMER_NOMBRE;
        this.primerApellido=PRIMER_APELLIDO;
        this.segundoNombre=SEGUNDO_NOMBRE;
        this.segundoApellido=SEGUNDO_APELLIDO;
        this.numeroIdentificacion=NUMERO_IDENTIFICACION;
        this.tipoIdentificacion=TIPO_IDENTIFICACION;
        this.numeroTelefono=NUMERO_TELEFONO;
    }

    public ClienteTestDataBuilder conId(Long id){
        this.id=id;
        return this;
    }

    public ClienteTestDataBuilder conPrimerNombre(String primerNombre){
        this.primerNombre=primerNombre;
        return this;
    }

    public ClienteTestDataBuilder conPrimerApellido(String primerApellido){
        this.primerApellido=primerApellido;
        return this;
    }

    public ClienteTestDataBuilder conSegundoNombre(String segundoNombre){
        this.segundoNombre=segundoNombre;
        return this;
    }

    public ClienteTestDataBuilder conSegundoApellido(String segundoApellido){
        this.segundoApellido=segundoApellido;
        return this;
    }

    public ClienteTestDataBuilder conNumeroIdentificacion(String numeroIdentificacion){
        this.numeroIdentificacion=numeroIdentificacion;
        return this;
    }

    public ClienteTestDataBuilder conTipoIdentificacion(TipoIdentificacion tipoIdentificacion){
        this.tipoIdentificacion=tipoIdentificacion;
        return this;
    }

    public ClienteTestDataBuilder conNumeroTelefono(String numeroTelefono){
        this.numeroTelefono=numeroTelefono;
        return this;
    }

    public Cliente builder(){
        return new Cliente(this.id,this.primerNombre,this.segundoNombre,this.primerApellido,this.segundoApellido,this.numeroIdentificacion,this.tipoIdentificacion,this.numeroTelefono);
    }
}
