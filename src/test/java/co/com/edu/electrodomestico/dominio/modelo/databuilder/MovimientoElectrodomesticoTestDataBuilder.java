package co.com.edu.electrodomestico.dominio.modelo.databuilder;

import co.com.edu.electrodomestico.dominio.modelo.*;

import java.time.LocalDate;

public class MovimientoElectrodomesticoTestDataBuilder {

    private static final Long ID=1L;
    private static final RegistroElectrodomestico REGISTRO_ELECTRODOMESTICO= new RegistroElectrodomestico(1L,new Cliente(1L,"Victor","Manuel","Vallejo","Loaiza","123456789",
            new TipoIdentificacion(1L,"cedula de ciudadania"),"3144568565"), LocalDate.of(2020,5,27),new TipoElectrodomestico(1L,"Inmusa","Cafetera"),"xxxx",LocalDate.of(2020,06,13));
    private static final String INFORMACION_SALIDA="xxxmmmyyyy";
    private static final boolean ESTADO=true;

    private Long id;
    private RegistroElectrodomestico registroElectrodomestico;
    private String informacionSalida;
    private boolean estado;

    public MovimientoElectrodomesticoTestDataBuilder() {
        this.id=ID;
        this.registroElectrodomestico=REGISTRO_ELECTRODOMESTICO;
        this.informacionSalida=INFORMACION_SALIDA;
        this.estado=ESTADO;
    }

    public MovimientoElectrodomesticoTestDataBuilder conId(Long id){
        this.id=id;
        return this;
    }

    public MovimientoElectrodomesticoTestDataBuilder conRegistroElectrodomestico(RegistroElectrodomestico registroElectrodomestico){
        this.registroElectrodomestico=registroElectrodomestico;
        return this;
    }

    public MovimientoElectrodomesticoTestDataBuilder conInformacionSalida(String informacionSalida){
        this.informacionSalida=informacionSalida;
        return this;
    }

    public MovimientoElectrodomesticoTestDataBuilder conEstado(boolean estado){
        this.estado=estado;
        return this;
    }

    public MovimientoElectrodomestico builder(){
        return new MovimientoElectrodomestico(this.id, this.registroElectrodomestico,this.informacionSalida, this.estado);
    }
}
