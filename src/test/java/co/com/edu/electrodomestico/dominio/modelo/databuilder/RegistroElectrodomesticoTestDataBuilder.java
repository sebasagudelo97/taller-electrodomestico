package co.com.edu.electrodomestico.dominio.modelo.databuilder;

import co.com.edu.electrodomestico.dominio.modelo.Cliente;
import co.com.edu.electrodomestico.dominio.modelo.RegistroElectrodomestico;
import co.com.edu.electrodomestico.dominio.modelo.TipoElectrodomestico;
import co.com.edu.electrodomestico.dominio.modelo.TipoIdentificacion;

import java.time.LocalDate;

public class RegistroElectrodomesticoTestDataBuilder {

    private static final Long ID=1L;
    private static final Cliente CLIENTE = new Cliente(1L,"Victor","Manuel","Vallejo","Loaiza","123456789",
            new TipoIdentificacion(1L,"cedula de ciudadania"),"3144568565");
    private static final LocalDate FECHA_INGRESO= LocalDate.of(2020,05,27);
    private static final TipoElectrodomestico TIPO_ELECTRODOMESTICO= new TipoElectrodomestico(1L,"RRR","tipo");
    private static final String REPORTE_DAÑO="xxxx";
    private static final LocalDate FECHA_SALIDA=LocalDate.of(2020,06,13);

    private Long id;
    private Cliente cliente;
    private LocalDate fechaIngreso;
    private TipoElectrodomestico tipoElectrodomestico;
    private String reporteDaño;
    private LocalDate fechaSalida;

    public RegistroElectrodomesticoTestDataBuilder() {
        this.id=ID;
        this.cliente=CLIENTE;
        this.fechaIngreso=FECHA_INGRESO;
        this.tipoElectrodomestico=TIPO_ELECTRODOMESTICO;
        this.reporteDaño=REPORTE_DAÑO;
        this.fechaSalida=FECHA_SALIDA;
    }

    public RegistroElectrodomesticoTestDataBuilder conId(Long id){
        this.id=id;
        return this;
    }

    public  RegistroElectrodomesticoTestDataBuilder conCliente(Cliente cliente){
        this.cliente=cliente;
        return this;
    }

    public RegistroElectrodomesticoTestDataBuilder conFechaIngreso(LocalDate fechaIngreso){
        this.fechaIngreso=fechaIngreso;
        return this;
    }

    public RegistroElectrodomesticoTestDataBuilder conTipoElectrodomestico(TipoElectrodomestico tipoElectrodomestico){
        this.tipoElectrodomestico=tipoElectrodomestico;
        return this;
    }

    public RegistroElectrodomesticoTestDataBuilder conReporteDaño(String reporteDaño){
        this.reporteDaño=reporteDaño;
        return this;
    }

    public RegistroElectrodomesticoTestDataBuilder conFechaSalida(LocalDate fechaSalida){
        this.fechaSalida=fechaSalida;
        return this;
    }

    public RegistroElectrodomestico builder(){
        return new RegistroElectrodomestico(this.id,this.cliente,this.fechaIngreso,this.tipoElectrodomestico,this.reporteDaño,this.fechaSalida);
    }
}
