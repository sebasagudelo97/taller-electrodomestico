package co.com.edu.electrodomestico.dominio.modelo.databuilder;

import co.com.edu.electrodomestico.dominio.modelo.TipoElectrodomestico;

public class TipoElectrodomesticoTestDataBuilder {

    private static final Long ID=1L;
    private static final String MARCA="Inmusa";
    private static final String DESCRIPCION="Cafetera";

    private Long id;
    private String marca;
    private String descripcion;

    public TipoElectrodomesticoTestDataBuilder() {
        this.id=ID;
        this.marca=MARCA;
        this.descripcion=DESCRIPCION;
    }

    public TipoElectrodomesticoTestDataBuilder conId( Long id){
        this.id=id;
        return this;
    }

    public TipoElectrodomesticoTestDataBuilder conMarca( String marca){
        this.marca=marca;
        return this;
    }

    public TipoElectrodomesticoTestDataBuilder conDescripcion(String descripcion){
        this.descripcion=descripcion;
        return this;
    }

    public TipoElectrodomestico builder(){
        return new TipoElectrodomestico(this.id,this.marca,this.descripcion);
    }
}
