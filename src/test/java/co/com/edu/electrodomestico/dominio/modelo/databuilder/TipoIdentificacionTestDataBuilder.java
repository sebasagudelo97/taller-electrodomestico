package co.com.edu.electrodomestico.dominio.modelo.databuilder;

import co.com.edu.electrodomestico.dominio.modelo.TipoIdentificacion;

public class TipoIdentificacionTestDataBuilder {

    private static final Long ID=1L;
    private static final String DESCRIPCION="cedula de ciudadania";

    private Long id;
    private String descripcion;

    public TipoIdentificacionTestDataBuilder() {
        this.id=ID;
        this.descripcion=DESCRIPCION;
    }

    public TipoIdentificacionTestDataBuilder conId(Long id){
        this.id=id;
        return this;
    }

    public TipoIdentificacionTestDataBuilder conDescripcion(String descripcion){
        this.descripcion=descripcion;
        return this;
    }

    public TipoIdentificacion builder(){
        return new TipoIdentificacion(this.id, this.descripcion);
    }
}
